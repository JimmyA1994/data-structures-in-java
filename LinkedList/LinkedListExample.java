public class LinkedListExample {

	public static void main (String[] args) {	
		LinkedList ll = new LinkedList();	
		ll.insertSomeElements();
		System.out.println("Length of list is: "+ll.getLength());
		ll.printList();
		int x = 2;
		System.out.print("Searching for element "+x+"... ");
		boolean found = ll.searchElement(x);
		if(found) System.out.println("Element "+x+" was found in the list.");
		else System.out.println("The element "+x+" was NOT found in the list.");
		x=0;
		System.out.print("Deleting element "+x+"... ");
		found = ll.deleteElement(x);
		if(found) System.out.println("Element "+x+" was deleted.");
		else System.out.println("The element "+x+" was NOT found in the list.");
		x=2;
		System.out.print("Deleting element "+x+"... ");
		found = ll.deleteElement(x);
		if(found) System.out.println("Element "+x+" was deleted.");
		else System.out.println("The element "+x+" was NOT found in the list.");
		System.out.println("Length of list is: "+ll.getLength());
		ll.printList();
		System.out.print("Searching for element "+x+"... ");
		found = ll.searchElement(x);
		if(found) System.out.println("Element "+x+" was found in the list.");
		else System.out.println("The element "+x+" was NOT found in the list.");
	}
}
