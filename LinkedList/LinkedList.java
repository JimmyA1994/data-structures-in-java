class LinkedList {

	public static Node head;
	public static Node tail;
	public static int length=0;

	static class Node {
		
		private int data;
		private Node next;

		Node() {
			this.data=0;
			this.next=null;
		}

		Node(int d) {
			this.data=d;
			this.next=null;
		}

		void setData(int d) {
			this.data=d;
		}
		
		int getData() {
			return data;
		}

		void setNext(Node n) {
			this.next = n;
		}

		Node getNext() {
			return next;
		}
	}

	public static void LinkedList(){
		head = null;
		tail = head;
	}

	public static void LinkedList(int d){
		head = null;
		tail = head;
		insertElement(d);
	}

	public static void insertElement(int d) {		
		Node e = new Node(d);
		if(head==null){ // list is empty
			head=e;
			tail=e;
		}
		else{
			tail.setNext(e);
			tail = e;			
		}
		length++;
	}

	public static void insertSomeElements() {
		
		insertElement(0);
		insertElement(1);
		insertElement(2);
		insertElement(3);
	}

	public static void printList() {
		int count = 0;
		Node curr = head;
		while(curr!=null){
			System.out.println("Element "+(count++)+" has data "+curr.getData());
			curr=curr.getNext();
		}
	}

	public static boolean searchElement(int x) {
		boolean found = false;
		Node curr = head;
		while(curr!=null){
			if(curr.getData() == x) {
				found = true;
				break;
			}
			curr=curr.getNext();
		}
		return found;
	}

	public static boolean deleteElement(int x) {
		boolean found = false;
		Node prev = null;
		Node curr = head;
		while(curr!=null){
			if(curr.getData() == x) {			
				found = true;
				if(prev==null) { // head must be removed
					head = curr.getNext();
					curr.setNext(null);
				}
				else if(curr == tail) { // tail must be removed
					tail = prev;
					prev.setNext(null);
				}
				else {
					prev.setNext(curr.getNext());
					curr.setNext(null);
				}
				length--;
				break;
			}
			prev=curr;
			curr=curr.getNext();
		}
		return found;
	}

	public static int getLength() {
		return length;
	}

}